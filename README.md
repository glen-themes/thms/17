![Screenshot preview of the theme "Strongest" by glenthemes](https://64.media.tumblr.com/fbe7f2f0bfa23d92943ffde02da991d4/tumblr_o8hww4gY1B1ubolzro2_r1_1280.png)

**Theme no.:** 17  
**Theme name:** Strongest  
**Theme type:** Free / Tumblr use  
**Description:** A tall sidebar theme with info boxes, featuring Saitama from One Punch Man.  
**Author:** @&hairsp;glenthemes  

**Release date:** 2016-06-09  
**Last updated:** 2023-05-06

**Post:** [glenthemes.tumblr.com/post/145651113619](https://glenthemes.tumblr.com/post/145651113619)  
**Preview:** [glenthpvs.tumblr.com/strongest](https://glenthpvs.tumblr.com/strongest)  
**Download:** [pastebin.com/vE8ffbas](https://pastebin.com/vE8ffbas)
